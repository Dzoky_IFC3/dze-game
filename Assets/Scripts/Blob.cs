﻿using UnityEngine;
using System.Collections;

public class Blob : MonoBehaviour {
    private float time;
    private float gtime;
    private bool red;
    private float show;
    private float hide;
    public bool hides;
    private Animator animator;
    // Use this for initialization
    void Start () {
        transform.position = new Vector3(GameManager.instance.Blobpos.x, GameManager.instance.Blobpos.y, 0f);
        animator = GetComponent<Animator>();
        if (hides)
            red = true;
        else
            red = false;
    }
    public void ShowBlob() {
        red = false;
        show = Time.time + 0.15f;
    
      //  show = Time.time + 0.2f;
    }
    public void HideBlobl() {
        red = true;
        hide = Time.time + 0.15f;

    }

    public void Damage() {
        time = Time.time + 1f;
    }
    public void Heal()
    {
        gtime = Time.time + 1f;
    }
    // Update is called once per frame
    void Update () {
        if(GameObject.Find("Player").GetComponent<Player>().trig)
            animator.SetTrigger("blobrun");
            if (transform.position.x > GameManager.instance.Blobpos.x)
            {
                transform.position = new Vector3(transform.position.x - 0.03f, transform.position.y, transform.position.z);
            }
        if ((Time.time < time) && red==false)
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), 1.0f);
        else if ((Time.time < gtime) && red == false)
            gameObject.GetComponent<SpriteRenderer>().color = new Color(Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), 1.0f, Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), 1.0f);

        else if (hides == false && red == false)  gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);  
        else if (hides && red) gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        else if (hides && red == false) gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        else if (hides == false && red) gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        // Mathf.Round(Mathf.PingPong(Time.time * 2, 1.0f));
        if (Time.time < hide)
            red = true;
        else if (Time.time < show)
            red = false;
        else if (hides)
            red = true;
        else
            red = false;

    }
}
