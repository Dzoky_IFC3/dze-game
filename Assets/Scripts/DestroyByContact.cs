﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DestroyByContact : MonoBehaviour {
    public GameObject bombexplosion;
    public GameObject smallexplosion;
    public GameObject thingexplosion;
    public GameObject foodexplosion;
    private bool timing = false;
    private bool scaling = false;
    private float time = 0.0f;
    private bool cameback = false;
    private Vector3 scale;

    // Use this for initialization

    void Start()
    {
        scale = new Vector3(gameObject.transform.localScale.x, gameObject.transform.localScale.y,
            gameObject.transform.localScale.z);
        gameObject.transform.localScale = new Vector3(0f, 0f, 1f);
        gameObject.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
       // Debug.Log(gameObject.transform.rotation);
    }

    void resetobject()
    {
        transform.localRotation = Quaternion.identity;
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0.0f;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Player playerScript = GameObject.Find("Player").GetComponent<Player>();
        Dog dogScript = GameObject.Find("Dog").GetComponent<Dog>();
        Blob blobscript = GameObject.Find("Blob").GetComponent<Blob>();
        if (other.tag == "Player")
        {
            /*  resetobject();
                                                            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * 200);
                                                        gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * 150);*/


            //  Debug.Log(GameObject.Find("Player").GetComponent<Player>().bombtime);
            switch (tag)
            {
                case "Bomb":
                    GameObject.Find("Player").GetComponent<Player>().bombtimer = false;
                    break;
                case "Food":
                    GameObject.Find("Player").GetComponent<Player>().foodtimer = false;
                    break;
                case "Thing":
                    GameObject.Find("Player").GetComponent<Player>().thingtimer = false;
                    break;
            }
            //  GameObject.Find("Player").GetComponent<Player>().bombtime = 0.0f;
        }

        switch (tag)
        {
            case "Bomb":
             
                if (playerScript.ibomb == true && playerScript.ithing == false && playerScript.ifood == false && other.tag == "Player")
                {
                    Vibration.Vibrate(50);
                    PointsManager.instance.AddPoints(1);
                    resetobject();
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up*Random.Range(80, 120));
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right*Random.Range(300, 500));
                    SoundManager.instance.RandomizeSfx(playerScript.bombpunchSound);
                    cameback = true;
                }
                else if ((playerScript.ibomb == false && playerScript.ithing == false && playerScript.ifood == true && other.tag == "Player") || (playerScript.ibomb == false && playerScript.ithing == true && playerScript.ifood == false && other.tag == "Player"))
                {
                    Instantiate(bombexplosion, new Vector3(-1.6f, -0.05f, -0.65f), Quaternion.identity);
                    gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.0f);
                    SoundManager.instance.RandomizeSfx(playerScript.bombexplosionSound);
                    playerScript.Damage();
                    Destroy(gameObject);
                }
                else if (cameback && other.tag == "Blob")
                {
                    Instantiate(smallexplosion, new Vector3(GameObject.Find("Blob").transform.position.x + 0.2f, GameObject.Find("Blob").transform.position.y, GameObject.Find("Blob").transform.position.z), Quaternion.Euler(0, 180, 0));
                    blobscript.Damage();
                    SoundManager.instance.RandomizeSfx(playerScript.bombexplosionSound);
                    Destroy(gameObject);
                }
                break;
            case "Food":
        ;
                if (other.tag == "Dog")
                {
                    dogScript.dogJump();
                    SoundManager.instance.RandomizeSfx(playerScript.dogbarkSound);
                }
                else if (playerScript.ibomb && playerScript.ithing == false && playerScript.ifood == false && other.tag == "Player")
                {
                    resetobject();
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.3f, gameObject.transform.position.z);
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up*Random.Range(80, 120));
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right*Random.Range(300, 500));
                    SoundManager.instance.RandomizeSfx(playerScript.bombpunchSound);
                    dogScript.Damage();
                    cameback = true;
                }
                else if (playerScript.ibomb == false && playerScript.ithing && playerScript.ifood == false && other.tag == "Player")
                {
                    resetobject();
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.3f, gameObject.transform.position.z);
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up*Random.Range(150, 250));
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right*Random.Range(-70, -150));
                    SoundManager.instance.RandomizeSfx(playerScript.thingpunchSound);
                    dogScript.Damage();
                    timing = true;
                }
                else if (playerScript.ibomb == false && playerScript.ithing == false && playerScript.ifood == true && other.tag == "Player")
                {
                    PointsManager.instance.AddPoints(1);
                    Vibration.Vibrate(50);
                    Instantiate(foodexplosion, new Vector3(gameObject.transform.position.x - 0.1f, gameObject.transform.position.y - Random.Range(0.0f, 0.1f), gameObject.transform.position.z), Quaternion.identity);
                    scaling = true;
                }
                else if (cameback && other.tag == "Blob")
                {
                    Instantiate(foodexplosion, new Vector3(gameObject.transform.position.x - 0.1f, gameObject.transform.position.y, gameObject.transform.position.z), Quaternion.Euler(0, 180, 0));
                    blobscript.Heal();
                    scaling = true;
                }
                break;
            case "Thing":
               
                if (playerScript.ithing == true && playerScript.ibomb == false && playerScript.ifood == false && other.tag == "Player")
                {
                    PointsManager.instance.AddPoints(1);
                    Vibration.Vibrate(50);
                    resetobject();
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up*Random.Range(150, 250));
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right*Random.Range(-70, -150));
                    SoundManager.instance.RandomizeSfx(playerScript.thingpunchSound);
                    timing = true;
                }
                else if (playerScript.ibomb == false && playerScript.ithing == false && playerScript.ifood == true && other.tag == "Player")
                {
                    SoundManager.instance.RandomizeSfx(playerScript.bombexplosionSound);
                    float range = Random.Range(0.1f, 0.4f);
                    Instantiate(thingexplosion, new Vector3(gameObject.transform.position.x - 0.4f, gameObject.transform.position.y - range, gameObject.transform.position.z), Quaternion.identity);
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x - 0.4f, gameObject.transform.position.y - range, gameObject.transform.position.z);
                    playerScript.Damage();
                    scaling = true;
                }
                else if (playerScript.ibomb == true && playerScript.ithing == false && playerScript.ifood == false && other.tag == "Player")
                {
                    resetobject();
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up*Random.Range(80, 120));
                    gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right*Random.Range(300, 500));
                    SoundManager.instance.RandomizeSfx(playerScript.bombpunchSound);
                    playerScript.Damage();
                    cameback = true;
                }
                else if (cameback && other.tag == "Blob")
                {
                    Instantiate(thingexplosion, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z), Quaternion.Euler(0, 180, 0));
                    blobscript.Heal();
                    scaling = true;
                }
                break;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timing)
        {
            time += Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, gameObject.GetComponent<SpriteRenderer>().color.a - 0.05f);
        }
        if (time > 1f)
        {
            Destroy(gameObject);
        }
        if (scaling)
        {
            resetobject();
            gameObject.transform.Rotate(Vector3.forward*Time.deltaTime*300);
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.05f, gameObject.transform.localScale.y - 0.05f, gameObject.transform.localScale.z);
        }
        if (gameObject.transform.localScale.x < 0.0f)
        {
            Destroy(gameObject);
        }
    }
}
