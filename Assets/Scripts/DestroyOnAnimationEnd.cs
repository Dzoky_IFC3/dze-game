﻿using UnityEngine;
using System.Collections;

public class DestroyOnAnimationEnd : MonoBehaviour {
    public bool rotate = true;
    private Animator animator;
    private bool cya = false;
    private float minus;
    public float movementspeed = 0.0f;
    public bool scrollrightt = false;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        if (rotate)
        {
            transform.Rotate(Vector3.forward * Random.Range(0, 360));
            minus = Random.Range(02f, 0.4f);
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - minus, gameObject.transform.localScale.y - minus, gameObject.transform.localScale.z);
        }
    }

    // Update is called once per frame
    void Update () {
        if (cya) {
            if(!scrollrightt)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x-(0.2f+movementspeed),gameObject.transform.position.y-0.05f,gameObject.transform.position.z);
            else if(scrollrightt)
                gameObject.transform.position = new Vector3(gameObject.transform.position.x + (0.1f + movementspeed), gameObject.transform.position.y - 0.05f, gameObject.transform.position.z);
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, gameObject.GetComponent<SpriteRenderer>().color.a-0.2f);
        }
        if((gameObject.transform.position.x < -4) || (gameObject.transform.position.x > 4))
            Destroy(gameObject);
    }
    void DestroyAnimationObject() {
        Destroy(gameObject);
    }
    void Stopanim() {
        animator.Stop();
        cya = true;
    }
    void Cya() {
        cya = true;
    }
}
