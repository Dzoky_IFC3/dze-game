﻿using UnityEngine;
using System.Collections;

public class Dog : MonoBehaviour {
    private Animator animator;                  //Used to store a reference to the Player's animator component.
    private Animation anim;
    private float time;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }
    public void Damage()
    {
        time = Time.time + 0.5f;
    }
    public void dogJump()
    {
        animator.SetTrigger("Dog_Jump");
    }
    // Update is called once per frame
    void Update () {
        if (Time.time < time)
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), 1.0f);
        else
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

       if( GameObject.Find("Player").GetComponent<Player>().trig)
        {
            animator.SetTrigger("Dog_Run");
        }

    }
}
