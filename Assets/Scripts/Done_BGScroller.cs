﻿using UnityEngine;
using System.Collections;

public class Done_BGScroller : MonoBehaviour
{
	public float scrollSpeed;
	public float tileSizeZ;
    public string direction = "where at m8";
    public bool alwayScroll;
    private float time = 0.0f;

    private Vector3 startPosition;

	void Start ()
	{
        startPosition = transform.position;  
    }
    void scroll() {
        float newPosition = Mathf.Repeat((Time.time - time) * scrollSpeed, tileSizeZ);
        switch (direction)
        {
            case "left":
                transform.position = startPosition + Vector3.left * newPosition;
                break;
            case "right":
                transform.position = startPosition + Vector3.right * newPosition;
                break;
            case "up":
                transform.position = startPosition + Vector3.up * newPosition;
                break;
            case "down":
                transform.position = startPosition + Vector3.down * newPosition;
                break;
            default:
                transform.position = startPosition + Vector3.left * newPosition;
                break;
        }
    }
    void Update()
    {
        if (alwayScroll == false)
        {
            if (GameObject.Find("Player").GetComponent<Player>().trig == true) {
                    scroll();
                }
            else if (GameObject.Find("Player").GetComponent<Player>().trig == false) {
                time += Time.deltaTime;
            }
        }
        else if(alwayScroll) { 
            scroll();
        }
    }
    
}