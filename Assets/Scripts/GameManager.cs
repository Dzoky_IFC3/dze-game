﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;
  
   // private float timer;
    // Use this for initialization
    public int level = 1;
    public Vector2 Blobpos = new Vector2(1.60f, -0.12f);
    public Vector2 bombfor = new Vector2(160f, -170f);
    public Vector2 thingfor = new Vector2(170f, -170f);
    public Vector2 foodfof = new Vector2(135f, -170f);
    public Vector2 bombpos = new Vector2(1.5f, 0.4f);
    public Vector2 thingpos = new Vector2(1.5f, 0.4f);
    public Vector2 foodpos = new Vector2(1.5f, 0.4f);
    void Awake ()
    {
      //  timer = Time.time + 5.0f;
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
      //  DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	 /*   if (timer < Time.time)
	    {
	        Blobpos.x = 1;
	        timer += 10f;
	    }*/
	}

    public void Level2()
    {
         Blobpos = new Vector2(1.28f, -0.12f);
         thingfor = new Vector2(185f, -140f);
         bombfor = new Vector2(150f, -160f);
         foodfof = new Vector2(125f, -160f);
}
    public void Level3()
    {
        Blobpos = new Vector2(0.96f, -0.12f);
        thingfor = new Vector2(150f, -150f);
        bombfor = new Vector2(140f, -150f);
        foodfof = new Vector2(115f, -150f);
    }
    public void Level4()
    {
        Blobpos = new Vector2(0.64f, -0.12f);
        thingfor = new Vector2(120f, -140f);
        bombfor = new Vector2(110f, -140f);
        foodfof = new Vector2(95f, -130f);
    }
    public void Level5()
    {
        Blobpos = new Vector2(0.32f, -0.12f);
        thingfor = new Vector2(160f, -90f);
        bombfor = new Vector2(110f, -120f);
        foodfof = new Vector2(80f, -110f);
    }
    public void Level6()
    {
        Blobpos = new Vector2(0f, -0.12f);
        thingfor = new Vector2(170f, -70f);
        bombfor = new Vector2(170f, -65f);
        foodfof = new Vector2(80f, -80f);
    }
}
