﻿using UnityEngine;
using System.Collections;

public class LPSTick : MonoBehaviour
{
    private bool rotate;
    private bool twitch;
    private float timer;
    private bool up;
    private int one;
    public GameObject LPStatic;
	// Use this for initialization
	void Start () {
	    //transform.rotation = new Vector3();
        gameObject.transform.rotation = Quaternion.Euler(0, 0, 45f);
    }
	
	// Update is called once per frame
	void Update () {
        // GameObject.Find("Player").GetComponent<Player>().trig
	    if (Input.GetKeyDown(KeyCode.C) && (!GameObject.Find("Player").GetComponent<Player>().trig))
	    {
	        rotate = true;
            Instantiate(LPStatic);
        }
        for (var i = 0; i < Input.touchCount; ++i)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began && (!GameObject.Find("Player").GetComponent<Player>().trig))
            {
                // Need to put .x
                if (touch.position.x > (Screen.width / 2))
                {
                    rotate = true;
                    Instantiate(LPStatic);
                }
                else if (touch.position.x < (Screen.width / 2))
                {
                    rotate = true;
                    Instantiate(LPStatic);
                }
            }
        }

    }

    void FixedUpdate()
    {
        Quaternion originalRot = transform.rotation;
        if (twitch)
        {
            if (timer < Time.time)
            {
              
                if (!up)
                {
                    up = true;
                    transform.rotation = originalRot*Quaternion.AngleAxis(0.5f, Vector3.back);
                    one += 1;
                }
                else if (up)
                {
                    up = false;
                    transform.rotation = originalRot * Quaternion.AngleAxis(-0.5f, Vector3.back);
                    one += 1;
                }
                if (one == 4)
                {
                    one = 0;
                    timer += 1.05f;
                }
                else timer += 0.05f;
            }
        }
        if (rotate)
            if (gameObject.transform.rotation.z > 0.01f)
            {
                transform.rotation = originalRot*Quaternion.AngleAxis(-1, Vector3.forward);
                
            }
            else
            {
                GameObject.Find("Player").GetComponent<Player>().trig = true;
                rotate = false;
                twitch = true;
                timer = Time.time + 1.0f;
            }
       // Debug.Log(gameObject.transform.rotation.z);
    }
}
