﻿using UnityEngine;
using System.Collections;

public class Startrotate : MonoBehaviour
{
    private Animator anim;
    [HideInInspector]
    public bool started;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	    started = false;
        transform.position = new Vector3(3f, transform.position.y, transform.position.z);
        transform.localScale = new Vector3(transform.localScale.x+0.05f, transform.localScale.y+0.05f, transform.localScale.z);
    }
	
	// Update is called once per frame
	void Update () {
        if(transform.position.x > 2f)
            transform.position = new Vector3(transform.position.x - 0.03f, transform.position.y, transform.position.z);
        else if(transform.localScale.x > 1f)
            transform.localScale = new Vector3(transform.localScale.x - 0.01f, transform.localScale.y - 0.01f, transform.localScale.z);
        else if (!started) started = true;
        if (GameObject.Find("Player").GetComponent<Player>().trig )
	    {
	        anim.SetTrigger("spin");
	        Destroy(this);
	    }
	}
}
