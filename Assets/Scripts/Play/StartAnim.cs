﻿using UnityEngine;
using System.Collections;

public class StartAnim : MonoBehaviour {
    private Animator anim;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (GameObject.Find("Player").GetComponent<Player>().trig)
        {
            anim.SetTrigger("work");
            Destroy(this);
        }
    }
}
