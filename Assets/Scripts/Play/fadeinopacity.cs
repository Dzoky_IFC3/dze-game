﻿using UnityEngine;
using System.Collections;

public class fadeinopacity : MonoBehaviour
{
    public float opacity = 1.0f;
    public bool plus = true;
	// Use this for initialization
	void Start () {
        if(plus)
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
      //  else if(!plus)
           // gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }
	
	// Update is called once per frame
	void Update () {
	    if ((gameObject.GetComponent<SpriteRenderer>().color.a < opacity)&& plus)
	    {
	        if (GameObject.Find("LPrecord").GetComponent<Startrotate>().started)
	        {
	            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f,
	                gameObject.GetComponent<SpriteRenderer>().color.a + 0.05f);
	        }
	    }
        else if ((gameObject.GetComponent<SpriteRenderer>().color.b > opacity) && !plus)
	    {
            if (GameObject.Find("Player").GetComponent<Player>().trig)
            {
                gameObject.GetComponent<SpriteRenderer>().color = new Color(gameObject.GetComponent<SpriteRenderer>().color.r-0.05f, gameObject.GetComponent<SpriteRenderer>().color.g - 0.05f, gameObject.GetComponent<SpriteRenderer>().color.b - 0.05f,
                    gameObject.GetComponent<SpriteRenderer>().color.a);
            }
        }
	    else Destroy(this);
	}
}
