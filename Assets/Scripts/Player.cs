﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Player : MonoBehaviour {
    private Animator animator;                  //Used to store a reference to the Player's animator component.
    private Animation anim;
    public bool trig;
    private float timer;
    private bool time;
    private int count;
    public GameObject[] projectiles;
    [HideInInspector]
    public float bombtime = 0.0f;
    public float thingtime = 0.0f;
    public float foodtime = 0.0f;
    [HideInInspector]
    public bool bombtimer = false;
    public bool thingtimer = false;
    public bool foodtimer = false;
    public GameObject song;
    private bool isPlaying = false;
    public bool ibomb, ifood, ithing;
    private int bools = 0;
    public AudioClip bombexplosionSound;
    public AudioClip dogbarkSound;
    public AudioClip bombpunchSound;
    public AudioClip thingpunchSound;
    public AudioClip hiittingSound;
    public AudioClip hiittingSound1;
    public AudioClip shieldingSound;
    public AudioClip shieldingSound1;
    private float dtime;
    [HideInInspector]
    public float songtime = 0.0f;

   // public Text score;
    void Start () {
        //Get a component reference to the Player's animator component
        animator = GetComponent<Animator>();
        ifood = true;
        trig = false;
        count = 0;
        Instantiate(projectiles[1]);
        Instantiate(projectiles[0]);
        Instantiate(projectiles[2]);
        bombtimer = true;
        thingtimer = true;
        foodtimer = true;

        //gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1.0f);
        gameObject.GetComponent<Collider2D>().enabled = true;
        Blob blobsscript = GameObject.Find("Blobb").GetComponent<Blob>();
        //blobsscript.hideperm();
    }
    public void Damage()
    {
        dtime = Time.time + 0.5f;
    }
    public void play() {
            if (!isPlaying && trig) {
                    Instantiate(song);
                    isPlaying = true;
            }
        }
    void ibombenable() {
        ibomb = true;
        ifood = false;
        SoundManager.instance.RandomizeSfx(hiittingSound, hiittingSound1);
    }
    void ibombdisable() {
        ibomb = false;
        ifood = true;
    }
    void ithingenable() {
        ithing = true;
        ifood = false;
        SoundManager.instance.RandomizeSfx(shieldingSound1);
    }
    void ithingdisable() {
        ithing = false;
        ifood = true;
    }
    // Update is called once per frame
    void Update () {
        if (ibomb) { bools += 1; }
        if (ithing) { bools += 1; }
        if (ifood) { bools += 1; }
        if (bools > 1) { Debug.Log("Wuuuuut" + bools); bools = 0; }else{ bools = 0; }
        if (Time.time < dtime)
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, Mathf.Round(Mathf.PingPong(Time.time * 10, 1.0f)), Mathf.Round(Mathf.PingPong(Time.time * 10, 1.0f)), 1.0f);
     else
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        play();
       
        /*    if (Input.GetKeyDown(KeyCode.Z))
            {
                ibomb = true;

            }
            else if (Input.GetKeyUp(KeyCode.Z))
            {
                ibomb = false;
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                ithing = true;

            }
            else if (Input.GetKeyUp(KeyCode.X))
            {
                ithing = false;
            }*/
   /*     if (ibomb == false && ithing == false)
        {
            ifood = true;
        }
        else { ifood = false; }*/
       // Debug.Log("Bomb: " + ibomb + ". Thing:" + ithing + ". Food:" + ifood);
        /* AnimatorClipInfo[] animationClip = animator.GetCurrentAnimatorClipInfo(0);
         int currentFrame = (int)(animationClip[0].weight * (animationClip[0].clip.length * animationClip[0].clip.frameRate));
         Debug.Log(currentFrame);
         Debug.Log("Weight: " + animationClip[0].weight + " Length: " + animationClip[0].clip.length + " FrameRate: " + animationClip[0].clip.frameRate);*/
        /*  if (animator.GetCurrentAnimatorStateInfo(0).IsName("Dzo_Run") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime == 1.0f) {
              count += 1;
              Debug.Log(count + "Done");

          }*/

        if (Input.touchCount == 1)
        {
            // touch on screen
            if (Input.GetTouch(0).phase == TouchPhase.Began && (trig == false))
            {
                animator.SetTrigger("Dzo_Run");
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Began && (trig == true)) {
            //    animator.SetTrigger("Dzo_Hit");
                
              //  trig = false;
            }
        }

     /*   if (Input.GetButton("Fire1")) {
            Instantiate(bomb, new Vector3(1.59f, -0.12f, -0.21f), Quaternion.identity);
        }*/
        if(trig) /*(Input.GetKeyDown(KeyCode.C))*/
        {
            
            animator.SetTrigger("Dzo_Run");
            //trig = true;
           // animator.speed = 1.0f;

        }
        if (Input.GetKeyDown(KeyCode.X))
        {
           
            animator.SetTrigger("Dzo_Shield");
            
            trig = true;
            time = true;
            timer = 0.0f;
            // animator.speed = 1.0f;

        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            animator.SetTrigger("Dzo_Hit");
           
          

            time = false;
        //    Debug.Log(timer);
    
            //  animator.SetFloat("Dzo_Run", 5.0f);
            //  anim["Dzo_Run"].speed = 5.0f;
            //trig = false;
            //  animator.speed = 2.0f; // keisti greiciui

        }
        if (bombtimer)
        {
            bombtime += Time.deltaTime;
        }
        if (foodtimer)
        {
            foodtime += Time.deltaTime;
        }
        if (thingtimer)
        {
            thingtime += Time.deltaTime;
        }
        //  gameObject.GetComponent<Animation>
        //if animation is between frames 10 and 15
        /*     if (animation["attack"].time > 0.333 < animation["attack"].time < 0.5)
             {
                 gameObject.GetComponent<Animation>().["attack"].time;
             }*/
     /*   if (!thingtimer && !foodtimer && !bombtimer)
        {
            Debug.Log(bombtime +" " + thingtime +" " + foodtime);
        }*/
        if (time) {
            timer += Time.deltaTime;
        }
        for (var i = 0; i < Input.touchCount; ++i)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began)
            {
                // Need to put .x
                if (touch.position.x > (Screen.width / 2))
                {
                    animator.SetTrigger("Dzo_Shield");
                }
                else if (touch.position.x < (Screen.width / 2)) {
                    animator.SetTrigger("Dzo_Hit");
                   // score.text = "" + (Int32.Parse(score.text) + 1);
                }
            }
        }
    }
}
