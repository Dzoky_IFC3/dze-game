﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PointsManager : MonoBehaviour {
    public static PointsManager instance = null;     //Allows other scripts to call functions from SoundManager.			
    public Text text;
    void Awake()
    {
        text.text = "0";
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
      //  DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
    //  GameManager.instance.Level6();
        if ((Int32.Parse(text.text) >= 10) && GameManager.instance.level == 1)
           {
               GameManager.instance.level = 2;
               GameManager.instance.Level2();
           }
           if ((Int32.Parse(text.text) >= 20) && GameManager.instance.level == 2)
           {
               GameManager.instance.level = 3;
               GameManager.instance.Level3();
           }
        if ((Int32.Parse(text.text) >= 30) && GameManager.instance.level == 3)
        {
            GameManager.instance.level = 4;
            GameManager.instance.Level4();
        }
        if ((Int32.Parse(text.text) >= 40) && GameManager.instance.level == 4)
        {
            GameManager.instance.level = 5;
            GameManager.instance.Level5();
        }
        if ((Int32.Parse(text.text) >= 50) && GameManager.instance.level == 5)
        {
            GameManager.instance.level = 6;
            GameManager.instance.Level6();
        }
    }

    public void AddPoints(float points)
    {
        text.text = "" +(Int32.Parse(text.text) + points);
    }
}
