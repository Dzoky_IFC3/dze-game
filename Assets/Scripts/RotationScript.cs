﻿using UnityEngine;
using System.Collections;

public class RotationScript : MonoBehaviour {
    private int i = 0;
    // Use this for initialization
    void Start () {
        Blob blobscript = GameObject.Find("Blob").GetComponent<Blob>();
        Blob blobsscript = GameObject.Find("Blobb").GetComponent<Blob>();
        blobscript.HideBlobl();
        blobsscript.ShowBlob();
        float minus = Random.Range(0f, 0.2f);
        if (gameObject.tag == "Food")
        {
            gameObject.transform.position =new Vector3(GameManager.instance.Blobpos.x-0.1f, GameManager.instance.Blobpos.y + 0.52f, 0f);
            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * GameManager.instance.foodfof.x);
            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * GameManager.instance.foodfof.y);
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - (minus*0.8f), gameObject.transform.localScale.y - (minus*0.8f), gameObject.transform.localScale.z);
        }
        else if (gameObject.tag == "Bomb")
        {
            gameObject.transform.position = new Vector3(GameManager.instance.Blobpos.x - 0.1f, GameManager.instance.Blobpos.y + 0.52f, 0f);
            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * GameManager.instance.bombfor.x);
            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * GameManager.instance.bombfor.y);
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - minus, gameObject.transform.localScale.y - minus, gameObject.transform.localScale.z);
        } if (gameObject.tag == "Thing")
        {
            gameObject.transform.position = new Vector3(GameManager.instance.Blobpos.x - 0.1f, GameManager.instance.Blobpos.y + 0.52f, 0f);
            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * GameManager.instance.thingfor.x);
            gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * GameManager.instance.thingfor.y);
           
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - minus, gameObject.transform.localScale.y - minus, gameObject.transform.localScale.z);
        }
    }
  
	// Update is called once per frame
	void Update () {

        if (gameObject.tag == "Bomb")
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), Mathf.Round(Mathf.PingPong(Time.time * 8, 1.0f)), 1.0f);
        transform.Rotate(Vector3.forward * Time.deltaTime * 300);
        Scale();

    }
    void Scale() {
        if (i < 10) {
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, 1f);
            i++;
        }
    }
}
