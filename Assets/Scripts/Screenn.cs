﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Screenn : MonoBehaviour {

    public float orthographicSize;
    public float aspect;

    void Start()
 {
     Camera.main.projectionMatrix = Matrix4x4.Ortho(
             -orthographicSize* aspect, orthographicSize * aspect,
             -orthographicSize, orthographicSize,
            GetComponent<Camera>().nearClipPlane, GetComponent<Camera>().farClipPlane);
 }


// Update is called once per frame
void Update () {


    }


}
