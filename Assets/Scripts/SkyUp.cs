﻿using UnityEngine;
using System.Collections;

public class SkyUp : MonoBehaviour {
    private float time = 0.0f;
    private float timeunit;
    public float distance = 0.0019f;
    public float daliklis = 5.0f;
    public string side = "up";
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (time <= 0.0f)
        {
         //   Player playerScript = GameObject.Find("Player").GetComponent<Player>();
            time = Mathf.Round(GameObject.Find("Player").GetComponent<Player>().songtime);
            time = time / 100;
            timeunit = time/ daliklis;
        }
        if (time > 0.0f)
        {

            if (Time.time > time)
            {
                switch (side)
                {
                    case "up":
                        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + distance, gameObject.transform.position.z);
                        break;
                    case "right":
                        gameObject.transform.position = new Vector3(gameObject.transform.position.x + distance, gameObject.transform.position.y, gameObject.transform.position.z);
                        break;
                    case "rotate":
                        Quaternion originalRot = transform.rotation;
                        gameObject.transform.rotation = originalRot * Quaternion.AngleAxis(distance, Vector3.forward);
                        break;
                }
                
                time = Time.time + timeunit;
            }
        }
    }
}
