﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Song0 : MonoBehaviour {
    public GameObject[] projectiles;
    private List<float> gridPositions = new List<float>();  //A list of times
    float totalSeconds;
    AudioSource audioSource;
    public AudioClip sound;
    private int number = 0;
    private float time;
    private float timer;
    private float starttime = 1.344f;


    void Start ()
    {
      
        number = Mathf.RoundToInt(Random.Range(0, projectiles.Length));
        switch (number)
        {
            case 0:
                time = GameObject.Find("Player").GetComponent<Player>().thingtime;
                break;
            case 1:
                time = GameObject.Find("Player").GetComponent<Player>().bombtime;
                break;
            case 2:
                time = GameObject.Find("Player").GetComponent<Player>().foodtime;
                break;
        }

       
        GameObject.Find("Player").GetComponent<Player>().songtime = sound.length;
        audioSource = GetComponent<AudioSource>();
        //  audioSource.clip = sound;
        //  audioSource.pitch = 1f;
      // Debug.Log(sound.length);
   
        audioSource.Play();
        //audioSource.pitch = 0.25f;
        totalSeconds = 0.0f;
        gridPositions.Clear();

        //Loop through x axis (columns).

        //At each index add a new Vector3 to our list with the x and y coordinates of that position.
      // gridPositions.Add(0.5f);
        for (int i = 1; i < 100; i++)
        {
          
            gridPositions.Add(starttime);
            starttime += 0.448f;
        }
       /* gridPositions.Add(1.344f);
        gridPositions.Add(1.791f);
        gridPositions.Add(2.239f);
        gridPositions.Add(2.687f);
        gridPositions.Add(3.135f);
        gridPositions.Add(3.582f);
        gridPositions.Add(4.03f);
        gridPositions.Add(4.478f);
        gridPositions.Add(4.926f);
        gridPositions.Add(5.373f);
        gridPositions.Add(5.821f);
        gridPositions.Add(6.269f);
        gridPositions.Add(6.717f);
        gridPositions.Add(7.164f);
        gridPositions.Add(7.612f);
        gridPositions.Add(8.06f);
        gridPositions.Add(8.508f);
        gridPositions.Add(8.96f);
        gridPositions.Add(9.403f);
        gridPositions.Add(9.851f);
        gridPositions.Add(10.299f);
        gridPositions.Add(10.746f);
        gridPositions.Add(11.194f);
        gridPositions.Add(11.642f);
        gridPositions.Add(12.090f);*/
    }


    // Update is called once per frame
    void Update ()
    {
        timer += Time.deltaTime;
       // Debug.Log(audioSource.time + " " + (audioSource.timeSamples/(sound.frequency*0.1))/10);
      //  Debug.Log(audioSource.time + " " + (audioSource.timeSamples * (1.0f / sound.frequency)));
       // if (audioSource.pitch < 1) audioSource.pitch += 0.01f;
        // Debug.Log(GameObject.Find("Player").GetComponent<Player>().bombtimer + " " + GameObject.Find("Player").GetComponent<Player>().bombtime);
        if (gridPositions.Count != 0)
        {
           // Debug.Log("atimti " + GameObject.Find("Player").GetComponent<Player>().bombtime);
            if ((gridPositions[0]-time) <= totalSeconds)
            {
                Debug.Log(gridPositions[0] + " " + (gridPositions[0]-time));
             //   Debug.Log("aaa " + gridPositions[0]);
                // Instantiate(shot[Random.Range(0, shot.Length)]);
                Instantiate(projectiles[number]);
              //  PointsManager.instance.AddPoints(timer);
                timer = 0.0f;
                number = Mathf.RoundToInt(Random.Range(0, projectiles.Length));
                switch (number)
                {
                    case 0:
                        time = GameObject.Find("Player").GetComponent<Player>().thingtime;
                        break;
                    case 1:
                        time = GameObject.Find("Player").GetComponent<Player>().bombtime;
                        break;
                    case 2:
                        time = GameObject.Find("Player").GetComponent<Player>().foodtime;
                        break;
                }
                gridPositions.RemoveAt(0);
            }
        }
        totalSeconds += Time.deltaTime;

    }


}
