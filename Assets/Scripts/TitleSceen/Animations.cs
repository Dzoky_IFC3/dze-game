﻿using UnityEngine;
using System.Collections;

public class Animations : MonoBehaviour {
    Animator animator;
    private float timer;
	// Use this for initialization
	void Start () {

        animator = GetComponent<Animator>();
        timer = Random.Range(5,10) + Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        if (timer < Time.time) {
            float d = Mathf.Round(Random.Range(0, 3));
            if (d < 1)
                animator.SetTrigger("point");
            else if (d < 2)
                animator.SetTrigger("itch");
            else if (d < 3)
                animator.SetTrigger("balance");
            timer = Random.Range(5, 10) + Time.time;
        }
    }
    void Rotateast() {
        //Rotateondemand astScript = GameObject.Find("Asteroid").GetComponent<Rotateondemand>();
        GameObject.Find("Asteroid").GetComponent<Rotateondemand>().Rotate();
    }
}
