﻿using UnityEngine;
using System.Collections;

public class RandomShow : MonoBehaviour {
    private float timer;
    private float show;
	// Use this for initialization
	void Start () {
        timer = Time.time + 2.0f;
        show = Time.time;
      GetComponent<Renderer>().material.color = new Color(1,1, 1,0); //C#
  
	}
	
	// Update is called once per frame
	void Update () {
        if (timer < Time.time) {
            timer = Time.time + Random.Range(2,8);
            show = Time.time + 0.2f;
        }
        if (show > Time.time)
        {
            GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1); //C#
        }
        else {
            GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
        }
	}
}
