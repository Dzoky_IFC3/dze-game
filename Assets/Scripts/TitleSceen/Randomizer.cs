﻿using UnityEngine;
using System.Collections;

public class Randomizer : MonoBehaviour {
    Transform bcg;
    Animator anim;
    private float normalizedTime = 0;
    float alphaLevel;
    bool visible;
    bool invisible;
    float delay = 0.0f;
    // Use this for initialization
    void Start () {
        anim = gameObject.GetComponent<Animator>();
        float t = Random.Range(0.0f, 1.0f);
        float x = Random.Range(-2.6f, 15.6f);
        float y = Random.Range(-0.5f, 2.5f);
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - t, gameObject.transform.localScale.y - t, gameObject.transform.localScale.z);
        gameObject.transform.position = new Vector3(x, y, 0.0f);
        bcg = (GameObject.Find("Starb")).transform;
        transform.parent = bcg;
        float g = Random.Range(0.0f, 1.0f);
        jumpToTime(currentAnimationName(), g);
        alphaLevel = 0.7f;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, alphaLevel, 0.8f);
        visible = false;
        invisible = true;
        delay = Random.Range(0, 1.5f) + Time.time;
        float c = Random.Range(0.5f, 1.5f);
        anim.speed = c;
        // AnimationState clipState = GetComponent<Animation>().GetClip("Star");
        //  GetComponent<Animator>()["Sat"].time = 2f;
        /*  float desired_play_time = 2.3;
          animation["MyAnimation"].time = desired_play_time;
          animation["MyAnimation"].speed = 0.0;
          animation.Play("MyAnimation");*/

    }

    // Update is called once per frame
    void Update () {
        if (delay < Time.time)
        {
            if (invisible)
            {
                if (alphaLevel <= 0.7f) { visible = true; invisible = false; }
                else
                {
                    alphaLevel -= 0.02f;
                }
            }
            else if (visible)
            {
                if (alphaLevel >= 1f) { invisible = true; visible = false; }
                else
                {
                    alphaLevel += 0.02f;
                }

            }
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, alphaLevel, 0.8f);
        }
    }
    void jumpToTime(string name, float nTime)
    {
        anim.Play(name, 0, nTime);
    }
    string currentAnimationName()
    {
        var currAnimName = "";
        foreach (AnimationClip clip in anim.runtimeAnimatorController.animationClips)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName(clip.name))
            {
                currAnimName = clip.name.ToString();
            }
        }

        return currAnimName;

    }
}
