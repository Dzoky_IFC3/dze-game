﻿using UnityEngine;
using System.Collections;

public class Rotateondemand : MonoBehaviour {
    private int[] ints;
    private int counter;
    private bool timer;
	// Use this for initialization
	void Start () {
        Rotate();
        int[] ints = { -5,0,-5,0 };
        counter = 0;
        timer = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.W)) {
            timer = true;
        }
        if (timer) {
            int[] ints = { -5, 0, -5, 0 };
            timer = false;
            if (counter < 4) {
                gameObject.transform.rotation = gameObject.transform.rotation = Quaternion.Euler(0, 0, ints[counter]);
                counter++;
                if (counter == 4) counter = 0;
            }
        }
	}
    public void Rotate() {
        timer = true;
    }
}
