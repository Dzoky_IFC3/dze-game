﻿using UnityEngine;
using System.Collections;

public class TitleScreenBump : MonoBehaviour {
    private float d;
    private float f;
    private float l;
    private float s;
    private int counter;
    private int minuscounter;
    private float timer;
    public float time = 1.0f;
    public float length = 0.02f;
    public float scsale = 0.2f;
    private bool scale;
    private float delay;
    private float opacity;
    private float countercount;
    private int[] ints;
    // Use this for initialization
    void Start () {
        delay = Random.Range(0, 0.5f) + Time.time;
        timer = Time.time + time;
        float ss = Random.Range(0, 5);
        if (ss > 2.5f)
            scale = true;
        else scale = false;
        int[] ints = {4,6,8 };

        countercount = ints[(int)Mathf.Round(Random.Range(0,3))];
	}

    // Update is called once per frame
    void Update()
    {
        if (delay < Time.time)
        {
            int g = (int)Mathf.Round(Mathf.PingPong(Time.time * 3, 1f));
            if (timer < Time.time)
            {
                if (counter < (countercount/2))
                {
                    if (counter == 0)
                    {
                        d = Random.Range(-length, length);
                        f = Random.Range(-length, length);
                        l = Random.Range(-scsale, scsale);
                        s = Random.Range(-scsale, scsale);
                        opacity = Random.Range(0, 0.1f);
                    }
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x + d, gameObject.transform.position.y + f, gameObject.transform.position.z);
                    gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, gameObject.GetComponent<SpriteRenderer>().color.a - opacity);
                    if (scale)
                    {
                        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + l, gameObject.transform.localScale.y + s, gameObject.transform.localScale.z);

                    }
                    else
                    {
                        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - l, gameObject.transform.localScale.y - s, gameObject.transform.localScale.z);

                    }
                    counter++;
                }
                else if (counter > ((countercount/2)-1) && counter < countercount)
                {
                    counter++;
                    gameObject.transform.position = new Vector3(gameObject.transform.position.x - d, gameObject.transform.position.y - f, gameObject.transform.position.z);
                    gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, gameObject.GetComponent<SpriteRenderer>().color.a + opacity);
                    if (scale)
                    {
                        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - l, gameObject.transform.localScale.y - s, gameObject.transform.localScale.z);

                    }
                    else
                    {
                        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + l, gameObject.transform.localScale.y + s, gameObject.transform.localScale.z);

                    }
                }
                else counter = 0;
            //    countercount = ints[(int)Mathf.Round(Random.Range(0, 3))];
                timer += time;
            }
            // gameObject.transform.position = new Vector3(gameObject.transform.position.x, d, gameObject.transform.position.z);
        }
    }
}
